using System;

namespace ITorrent
{

    public static class NetworkUtility
    {
        public static byte[] AddLong(byte[] msg, ulong number)
        {
            byte[] new_msg = new byte[msg.Length + 8];
            byte[] big_endian = BitConverter.GetBytes(number);

            Array.Reverse(big_endian);
            msg.CopyTo(new_msg, 0);
            big_endian.CopyTo(new_msg, msg.Length);

            return new_msg;
        }
        public static byte[] AddInteger(byte[] msg, uint number)
        {
            byte[] new_msg = new byte[msg.Length + 4];
            byte[] big_endian = BitConverter.GetBytes(number);

            Array.Reverse(big_endian);
            msg.CopyTo(new_msg, 0);
            big_endian.CopyTo(new_msg, msg.Length);

            return new_msg;
        }
        public static byte[] AddShort(byte[] msg, ushort number)
        {
            byte[] new_msg = new byte[msg.Length + 2];
            byte[] big_endian = BitConverter.GetBytes(number);

            Array.Reverse(big_endian);
            msg.CopyTo(new_msg, 0);
            big_endian.CopyTo(new_msg, msg.Length);

            return new_msg;
        }
        public static byte[] AddByte(byte[] msg, byte number)
        {
            byte[] new_msg = new byte[msg.Length + 1];

            msg.CopyTo(new_msg, 0);
            new_msg[new_msg.Length - 1] = number;

            return new_msg;
        }
        public static byte[] AddBytes(byte[] msg, byte[] bytes)
        {
            byte[] new_msg = new byte[msg.Length + bytes.Length];

            msg.CopyTo(new_msg, 0);
            bytes.CopyTo(new_msg, msg.Length);

            return new_msg;
        }
        public static byte[] AddBytesRevesed(byte[] msg, byte[] bytes)
        {
            byte[] new_msg = new byte[msg.Length + bytes.Length];

            msg.CopyTo(new_msg, 0);

            foreach (byte bt in bytes)
            {

            }

            return new_msg;
        }
        public static uint GetInteger(byte[] msg, int offset)
        {
            byte[] num = new byte[4];

            Array.Copy(msg, offset, num, 0, 4);
            Array.Reverse(num);

            return BitConverter.ToUInt32(num, 0);
        }
        public static ulong GetLong(byte[] msg, int offset)
        {
            byte[] num = new byte[8];

            Array.Copy(msg, offset, num, 0, 8);
            Array.Reverse(num);

            return BitConverter.ToUInt64(num, 0);
        }
        public static ushort GetShort(byte[] msg, int offset)
        {
            byte[] num = new byte[2];

            Array.Copy(msg, offset, num, 0, 2);
            Array.Reverse(num);

            return BitConverter.ToUInt16(num, 0);
        }
        public static byte[] GetBytes(byte[] msg, int offset, int length)
        {
            byte[] bytes = new byte[length];

            Array.Copy(msg, offset, bytes, 0, length);

            return bytes;
        }
    }
}