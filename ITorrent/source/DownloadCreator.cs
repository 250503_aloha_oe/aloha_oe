﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITorrent.Bencoding;
using ITorrent.GUI;

namespace ITorrent
{

    class DownloadCreator
    {
        List<Download> m_downloads;
        private byte[] m_peer_id;


        public DownloadCreator()
        {
            m_downloads = new List<Download>();
            m_peer_id = CreateID();
        }

        public void AddDownload(Download download)
        {
            m_downloads.Add(download);
        }

        public void AddDownload(BDictionary dict, List<bool> download_files, out Pause pause_del, out Continue continue_del, ChangeProgress p_del, ChangeSpeed s_del)
        {
            DownloadInfo new_info = new DownloadInfo(dict, m_peer_id, download_files);
            new_info.DownloadID = m_downloads.Count;

            Download new_download = new Download(new_info);

            new_download.SetProgressDel(p_del);
            new_download.SetSpeedDel(s_del);

            m_downloads.Add(new_download);
            new_download.Start();

            pause_del = new_download.Pause;
            continue_del = new_download.UnPause;

        }

        public void CloseAllDownloads()
        {
            foreach (var d in m_downloads)
                d.Close();

            m_downloads.Clear();
        }

        private byte[] CreateID()
        {
            byte[] peer_id = new byte[20];
            string id = "-AO0001-";
            Encoding.ASCII.GetBytes(id).CopyTo(peer_id, 0);

            Random rnd = new Random();
            for (int i = 0; i < 12; i++)
                peer_id[i + 8] = (byte)rnd.Next(256);

            return peer_id;
        }
    }
}

