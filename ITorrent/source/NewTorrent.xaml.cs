﻿using System;
using System.Collections.Generic;
using System.Windows;
using ITorrent.Bencoding;
using Microsoft.Win32;
using System.Windows.Controls;

namespace ITorrent.GUI
{
    public class Adding
    {
        public string Name { get; set; }
        public string Size { get; set; }
        public bool Selected { get; set; }
    }

    public partial class NewTorrent : Window
    {
        public bool Add { get; set; }
        private List<Adding> files = new List<Adding>();
        private List<bool> bool_list;

        public NewTorrent(BDictionary dict, ref List<bool> list)
        {
            bool_list = list;
            if ((dict["info"] as BDictionary).ContainsKey("files"))
            {
                int count = (dict["info"]["files"] as BList).Count;

                for (int i = 0; i < count; i++)
                {
                    int deep = (dict["info"]["files"][i]["path"] as BList).Count - 1;

                    long size = (dict["info"]["files"][i]["length"] as BInteger).Value;
                    string name = dict["info"]["files"][i]["path"][deep].ToString();

                    files.Add(new Adding() { Name = name, Size = FormatSize(size), Selected = false });
                }
            }
            else
            {
                string name = dict["info"]["name"].ToString();
                long size = (dict["info"]["length"] as BInteger).Value;

                files.Add(new Adding() { Name = name, Size = FormatSize(size), Selected = false });
            }

            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ListviewAdd.ItemsSource = files;
        }

        private void AddTorrent(object sender, RoutedEventArgs e)
        {
            foreach (Adding obj in files)
            {
                bool_list.Add(obj.Selected);
            }

            Add = true;
            this.Close();
        }

        private void IsSelected(object sender, RoutedEventArgs e)
        {
            CheckBox b = sender as CheckBox;
            Adding a = b.CommandParameter as Adding;

            files[files.IndexOf(a)].Selected ^= true;
        }

        private void CancelAdding(object sender, RoutedEventArgs e)
        {
            Add = false;
            this.Close();
        }

        private string FormatSize(long size)
        {
            double result = 0;

            if (size < 1000)
                return size.ToString("F2") + " B";
            else
            {
                result = size / Math.Pow(2, 10);
                if (result < 1000)
                    return result.ToString("F2") + " KB";
                else
                {
                    result = size / Math.Pow(2, 20);
                    if (result < 1000)
                        return result.ToString("F2") + " MB";
                    else
                    {
                        result = size / Math.Pow(2, 30);
                        return result.ToString("F2") + " GB";
                    }
                }
            }
        }
    }
}
