﻿using System;
using System.Text;
using System.Collections.Generic;

namespace ITorrent.Bencoding
{
    public class BCoder
    {
        public IBElement Decode(byte[] encoded_data)
        {
            int index = 0;
            try
            {
                return GetElement(encoded_data, ref index);
            }
            catch(IndexOutOfRangeException exc)
            {
                return null;
            }
        }

        private KeyValuePair<BString, IBElement> GetPair(byte[] data, ref int index)
        {
            BString key = GetString(data, ref index);
            IBElement value = GetElement(data, ref index);

            return new KeyValuePair<BString,IBElement>(key, value);
        }
        private BDictionary GetDictionary(byte[] data, ref int index)
        {
            index++;
            BDictionary dict = new BDictionary();
            while (data[index] != (byte)'e')
            {
                dict.Add(GetPair(data, ref index));
            }
            index++;
            return dict;
        }
        private BList GetList(byte[] data, ref int index)
        {
            index++;
            BList list = new BList();
            while (data[index] != (byte)'e')
            {
                list.Add(GetElement(data, ref index));
            }
            index++;
            return list;
        }
        private BInteger GetInteger(byte[] data, ref int index)
        {
            StringBuilder integer = new StringBuilder();
            index++;
            while (data[index] != (byte)'e')
            {
                integer.Append((char)data[index]);
                index++;
            }
            index++;
            return long.Parse(integer.ToString());
        }
        private BString GetString(byte[] data, ref int index)
        {
            StringBuilder size = new StringBuilder();
            while (data[index] != (byte)':')
            {
                size.Append((char)data[index]);
                index++;
            }
            int length = int.Parse(size.ToString());
            byte[] value = new byte[length];
            
            for(int i = 0; i < length; i++)
            {
                index++;
                value[i] = data[index];
            }
            index++;
            return value;
        }
        private IBElement GetElement(byte[] data, ref int index)
        {
            IBElement value = default(IBElement);
            switch (data[index])
            {
                case (byte)'i': value = GetInteger(data, ref index); break;
                case (byte)'l': value = GetList(data, ref index); break;
                case (byte)'d': value = GetDictionary(data, ref index); break;
                case (byte)'1':
                case (byte)'2':
                case (byte)'3':
                case (byte)'4':
                case (byte)'5':
                case (byte)'6':
                case (byte)'7':
                case (byte)'8':
                case (byte)'9':
                case (byte)'0': value = GetString(data, ref index); break;
            }
            return value;
        }
    }
}
