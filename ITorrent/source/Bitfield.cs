using System;

namespace ITorrent
{

    public class Bitfield
    {
        private int m_size;

        public byte[] BitField
        {
            get;
            private set;
        }

        public Bitfield(int size)
        {
            m_size = size;

            BitField = new byte[(size % 8) == 0 ? size / 8 : size / 8 + 1];
        }
        public Bitfield(int size, byte[] bytes)
        {
            m_size = size;

            BitField = bytes;
        }
        public void AddBit(int number)
        {
            if (number >= m_size)
                return;

            int byte_num = number / 8;

            BitField[byte_num] += (byte)Math.Pow(2, 7 - number % 8);
        }
        public bool HaveBit(int number)
        {
            if (number >= m_size)
                return false;

            int bit_number = 7 - number % 8;
            int byte_value = BitField[number / 8];

            for (int i = 7; i > bit_number; i--)
            {

                if (byte_value - (byte)Math.Pow(2, i) >= 0)
                {
                    byte_value -= (byte)Math.Pow(2, i);
                }
            }

            return byte_value >= (byte)Math.Pow(2, bit_number);
        }
        public void SetBitfield(byte[] bytes)
        {
            Array.Copy(bytes, 0, BitField, 0, (m_size % 8) == 0 ? m_size / 8 : m_size / 8 + 1);
        }
    }
}