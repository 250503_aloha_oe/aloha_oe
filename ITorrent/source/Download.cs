using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Timers;

namespace ITorrent
{
    public delegate void ChangeProgress(string name, float new_value);
    public delegate void ChangeSpeed(string name, float new_value);

    public enum DownloadState
    {
        Downloading,
        Pause,
        Completed,
    }

    public class Download
    {
        private int m_download_id;
        private DownloadInfo m_info;
        private TorrentHolder m_files_holder;
        private Thread m_thread;
        private DownloadState m_state;
        private MessageCreator m_message_creator;
        private TrackerRequest m_tracker_request;
        private bool m_stop_thread;

        private List<KeyValuePair<Message, Peer>> m_external_requests;
        private Dictionary<int, IncompletePiece> m_incomplete_pieces;
        private List<Peer> m_peers;
        private List<Peer> m_new_peers;
        private Dictionary<int, int> m_rarest_pieces;

        private ChangeProgress m_progress_del;
        private ChangeSpeed m_speed_del;

        private float m_progess;
        private float m_speed;
        private float m_progress_per_piece;

        public void SetSpeedDel(ChangeSpeed del)
        {
            m_speed_del = del;
        }
        public void SetProgressDel(ChangeProgress del)
        {
            m_progress_del = del;
        }

        public Download(DownloadInfo download_info)
        {
            m_info = download_info;
            m_files_holder = new TorrentHolder(ref m_info);
            m_tracker_request = new TrackerRequest(ref m_info);
            m_message_creator = new MessageCreator(ref m_info, ref m_files_holder);

            m_external_requests = new List<KeyValuePair<Message, Peer>>();
            m_incomplete_pieces = new Dictionary<int,IncompletePiece>();
            m_peers = new List<Peer>();
            m_new_peers = new List<Peer>();
            m_rarest_pieces = new Dictionary<int, int>();

            m_thread = new Thread(MainLoop);
            m_stop_thread = false;
            m_state = DownloadState.Downloading;
        }
        ~Download()
        {
            m_stop_thread = true;
            m_thread.Abort();
        }
        public void Close()
        {
            m_stop_thread = true;
            m_thread.Abort();
        }
        public void SaveToFile()
        {
            StreamReader sr = new StreamReader("log.txt");


        }
        public void MainLoop()
        {
            float speed = 0;
            float progress = 0;

            System.Timers.Timer responce_timer = new System.Timers.Timer(5000);
            System.Timers.Timer keep_alive_timer = new System.Timers.Timer(120000);
            System.Timers.Timer speed_timer = new System.Timers.Timer(15000);

            responce_timer.Elapsed += RespondToRequest;
            responce_timer.Start();

            keep_alive_timer.Elapsed += KeepAlive;
            keep_alive_timer.Start();

            InitRarestPieces();
            m_progress_per_piece = (float)100 / m_rarest_pieces.Count;

            UpdateTrackerPeers(ClientEvent.Started);

            while(!m_stop_thread)
            {
                bool state_changed = false;
    
                if(m_state == DownloadState.Downloading)
                {
                    IsFinished();

                    if(state_changed)
                    {
                        UpdateTrackerPeers(ClientEvent.None);
                        responce_timer.Start();
                        state_changed = false;
                    }

                    HandleIncomingMessages();
                    UpdateDownloadingPieces();
                    UpdatePeers();
                    SendRequests();
                    //SendToAll(m_message_creator.CreateKeepAliveMessage());
                }

                if(m_state == DownloadState.Pause)
                {
                    if (!state_changed)
                    {
                        state_changed = true;
                        responce_timer.Stop();
                        UpdateTrackerPeers(ClientEvent.Stopped);
                    }
                }

                Thread.Sleep(100);
            }

        }
        public void Start()
        {
            m_thread.Start();
        }
        public void Pause()
        {
            if (m_state == DownloadState.Downloading)
                m_state = DownloadState.Pause;
        }
        public void UnPause()
        {
            if (m_state == DownloadState.Pause)
                m_state = DownloadState.Downloading;
        }
        public DownloadState GetState()
        {
            return m_state;
        }
        public void AddPeer(Peer new_peer)
        {
            lock (m_new_peers)
            {
                m_new_peers.Add(new_peer);
            }
        }
        private void UpdateTrackerPeers(ClientEvent c_event)
        {
            var new_peers = m_tracker_request.SendRequest(c_event);

            if (c_event == ClientEvent.Stopped || c_event == ClientEvent.Completed || new_peers == null)
                return;

            for (int count = 0; count < new_peers.Count; count++)
            {
                bool equal = false;
                Peer new_peer = new Peer(new_peers[count].Item1, new_peers[count].Item2, m_info.TotalPiecesAmount);

                if(c_event == ClientEvent.None)
                    foreach (var old_peer in m_peers)
                    {
                        if (new_peer.Equals(old_peer))
                        {
                            equal = true;
                            break;
                        }
                    }

                if(!equal)
                {
                    new_peer.Send(m_message_creator.CreateHandshake());
                    new_peer.HandshakeSent = true;
                    new_peer.Send(m_message_creator.CreateBitfieldMessage());

                    if (c_event == ClientEvent.Started || m_state == DownloadState.Downloading)
                    {
                        new_peer.Send(m_message_creator.CreateUnchokeMessage());
                        new_peer.Send(m_message_creator.CreateIntrestedMessage());
                    }

                    m_peers.Add(new_peer);
                }
            }

           
        }
        private void UpdateDownloadingPieces()
        {
            List<int> keys_to_delete = new List<int>();

            foreach (var pair in m_incomplete_pieces)
            {
                if(pair.Value.IsComplete())
                {
                    if (!m_files_holder.AddPiece(pair.Key, pair.Value.Piece))
                        pair.Value.Reset();
                    else
                    {
                        m_progess += m_progress_per_piece;

                        m_progress_del(m_info.m_dictionary["info"]["name"].ToString(), m_progess);

                        keys_to_delete.Add(pair.Key);
                    }
                }
            }

            foreach (int key in keys_to_delete)
                m_incomplete_pieces.Remove(key);

            while (m_incomplete_pieces.Count < 10)
            {
                if (m_rarest_pieces.Count == 0)
                    break;

                var en = m_rarest_pieces.GetEnumerator();
                en.MoveNext();

                int rarest = en.Current.Key;

                m_rarest_pieces.Remove(rarest);
                if(rarest == m_info.TotalPiecesAmount - 1)
                {
                    long size = 0;
                    foreach (var obj in m_info.FilesSize)
                        size += obj;
                    int last_piece = (int)(size % m_info.PieceSize);

                    if (last_piece != 0)
                        m_incomplete_pieces.Add(rarest, new IncompletePiece(last_piece));
                    else
                        m_incomplete_pieces.Add(rarest, new IncompletePiece(m_info.PieceSize));
                }
                else
                    m_incomplete_pieces.Add(rarest, new IncompletePiece(m_info.PieceSize));
            }
        }
        private void HandleIncomingMessages()
        {
            for (int count = 0; count < m_peers.Count; count++)
            {
                byte[] incoming_msg;

                do
                {
                    try
                    {
                        incoming_msg = m_peers[count].Receive();
                    }
                    catch (NotImplementedException e)
                    {
                        incoming_msg = null;
                    }


                    if (incoming_msg != null)
                    {
                        Message msg = m_message_creator.DecryptMessage(incoming_msg);

                        HandleMessage(count, msg);
                    }
                }
                while (incoming_msg != null);
            }
        }
        private void HandleMessage(int peer_number, Message message)
        {
            if (message.Length == 0)
                return;

            if (message.Handshake)
                return;

            switch (message.Type)
            {
                case 0 : m_peers[peer_number].PeerChocking = true; break;
                case 1 : m_peers[peer_number].PeerChocking = false; break;
                case 2 : m_peers[peer_number].PeerIntrested = true; break;
                case 3 : m_peers[peer_number].PeerIntrested = false; break;
                case 4 :
                {
                    m_peers[peer_number].Bitfield.AddBit((int)message.Piece);

                    if (m_rarest_pieces.ContainsKey((int)message.Piece))
                        m_rarest_pieces[(int)message.Piece]++;

                    break;
                }
                case 5 :
                {
                    Bitfield bitfield = new Bitfield(m_info.TotalPiecesAmount, message.Payload);

                    for (int i = 0; i < m_info.TotalPiecesAmount; i++ )
                    {
                        if (m_rarest_pieces.ContainsKey(i) && bitfield.HaveBit(i))
                            m_rarest_pieces[i]++;
                            
                    }

                    m_peers[peer_number].Bitfield.SetBitfield(message.Payload);

                    break;
                }
                case 6 :
                {
                    if (!m_peers[peer_number].AmChocking)
                        m_external_requests.Add(new KeyValuePair<Message,Peer>(message, m_peers[peer_number]));

                    break;
                }
                case 7 :
                {
                    if(m_incomplete_pieces.ContainsKey((int)message.Piece))
                    {
                        m_incomplete_pieces[(int)message.Piece].AddBlock((int)message.Offset, message.Payload.Length, message.Payload);
                    }

                    break;
                }
                case 8 :
                {
                    message.Type = 6;

                    foreach(var pair in m_external_requests)
                    {
                        if(pair.Key.Equals(message))
                        {
                            m_external_requests.Remove(pair);
                            break;
                        }
                    }

                    break;
                }
                case 9 :
                {
                    //DHT shit
                    break;
                }
            }
        }
        private void RespondToRequest(object sender, ElapsedEventArgs e)
        {
            if(m_external_requests.Count > 0)
            {
                foreach(Peer peer in m_peers)
                {
                    if(m_external_requests[0].Value.Equals(peer))
                    {
                        Message msg = m_external_requests[0].Key;
                        byte[] block = m_files_holder.GetBlock((int)msg.Piece, (int)msg.Offset, (int)msg.Length);

                        if (block == null)
                            break;

                        byte[] message = m_message_creator.CreatePieceMessage(msg.Piece, msg.Offset, block);

                        peer.Send(message);
                    }
                }

                m_external_requests.RemoveAt(0);
            }
        }
        private void SendToAll(byte[] msg)
        {
            foreach (var peer in m_peers)
                peer.Send(msg);
        }
        private void InitRarestPieces()
        {
            List<bool> needed_pieces = m_files_holder.GetNeededPieces();

            for(int i = 0; i < needed_pieces.Count; i++)
            {
                if(needed_pieces[i])
                {
                    m_rarest_pieces.Add(i, 0);
                }
            }
        }
        private void UpdatePeers()
        {
            for(int i = 0; i < m_peers.Count; i++)
            {
                m_peers[i].Update();

                if(!m_peers[i].IsActive)
                {
                    m_peers.RemoveAt(i);
                    i--;
                }
            }
            foreach(var peer in m_new_peers)
            {
                peer.Send(m_message_creator.CreateBitfieldMessage());
                peer.Send(m_message_creator.CreateIntrestedMessage());
                peer.Send(m_message_creator.CreateUnchokeMessage());

                m_peers.Add(peer);
            }
           

            m_new_peers.Clear();
        }
        private void IsFinished()
        {
            if (m_files_holder.HaveAll())
            {
                m_state = DownloadState.Completed;
                m_files_holder.Unite();
            }
        }
        private void KeepAlive(object sender, ElapsedEventArgs e)
        {
            SendToAll(m_message_creator.CreateKeepAliveMessage());
        }
        private void SendRequests()
        {
            if (m_peers.Count == 0)
                return;

            foreach (var pair in m_incomplete_pieces)
            {
                for(int i = 0; i < m_peers.Count;i++)
                {
                    if(m_peers[i].Bitfield.HaveBit(pair.Key) && !m_peers[i].PeerChocking && m_peers[i].IsActive)
                    {
                        var missing = pair.Value.GetMissingBlock();

                        if (missing != null)
                        {
                            m_peers[i].Send(m_message_creator.CreateRequestMessage((uint)pair.Key, (uint)missing.Item1, (uint)missing.Item2));

                            var temp = m_peers[i];
                            m_peers[i] = m_peers[m_peers.Count - 1];
                            m_peers[m_peers.Count - 1] = temp;

                            i = -1;
                        }
                        else
                            break;
                            
                    }
                }

            }
        }
    }
}