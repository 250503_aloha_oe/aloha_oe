using System;
using System.Text;

namespace ITorrent
{

    public struct Message
    {
        public uint Type;
        public uint Length;
        public byte[] Payload;
        public uint Offset;
        public uint Piece;
        public uint Size;
        public ushort Port;
        public bool Handshake;

    }

    public class MessageCreator
    {
        private readonly TorrentHolder m_torrent_holder;
        private readonly DownloadInfo m_download_info;

        public MessageCreator(ref DownloadInfo d_info, ref TorrentHolder torrent_holder)
        {
            m_torrent_holder = torrent_holder;
            m_download_info = d_info;
        }

        public byte[] CreateHandshake()
        {
            int index = 1;
            byte[] handshake = new byte[49 + 19];

            handshake[0] = 19;

            Encoding.ASCII.GetBytes("BitTorrent protocol").CopyTo(handshake, index);
            index += 19;

            for (int i = 0; i < 8; i++)
                handshake[i + index] = 0;

            index += 8;

            m_download_info.InfoHash.CopyTo(handshake, index);
            index += 20;

            

            m_download_info.PeerID.CopyTo(handshake, index);

            return handshake;
        }
        public byte[] CreateKeepAliveMessage()
        {
            return NetworkUtility.AddInteger(new byte[0], 0);
        }
        public byte[] CreateChokeMessage()
        {
            return NetworkUtility.AddByte(NetworkUtility.AddInteger(new byte[0], 1), 0);
        }
        public byte[] CreateUnchokeMessage()
        {
            return NetworkUtility.AddByte(NetworkUtility.AddInteger(new byte[0], 1), 1);
        }
        public byte[] CreateIntrestedMessage()
        {
            return NetworkUtility.AddByte(NetworkUtility.AddInteger(new byte[0], 1), 2);
        }
        public byte[] CreateUnintrestedMessage()
        {
            return NetworkUtility.AddByte(NetworkUtility.AddInteger(new byte[0], 1), 3);
        }
        public byte[] CreateHaveMessage(uint piece_number)
        {
            return NetworkUtility.AddInteger(NetworkUtility.AddByte(NetworkUtility.AddInteger(new byte[0], 1), 4), piece_number);
        }
        public byte[] CreateBitfieldMessage()
        {
            byte[] msg = new byte[0];
            byte[] bitfield = m_torrent_holder.GetAsBitfield();

            msg = NetworkUtility.AddInteger(msg, 1 + (uint)bitfield.Length);
            msg = NetworkUtility.AddByte(msg, 5);
            msg = NetworkUtility.AddBytes(msg, bitfield);

            return msg;
        }
        public byte[] CreateRequestMessage(uint piece_number, uint offset, uint lenght)
        {
            byte[] msg = new byte[0];

            msg = NetworkUtility.AddInteger(msg, 13);
            msg = NetworkUtility.AddByte(msg, 6);
            msg = NetworkUtility.AddInteger(msg, piece_number);
            msg = NetworkUtility.AddInteger(msg, offset);
            msg = NetworkUtility.AddInteger(msg, lenght);

            return msg;
        }
        public byte[] CreatePieceMessage(uint piece_number, uint offset, byte[] block)
        {
            byte[] msg = new byte[0];

            msg = NetworkUtility.AddInteger(msg, (uint)(9 + block.Length));
            msg = NetworkUtility.AddByte(msg, 7);
            msg = NetworkUtility.AddInteger(msg, piece_number);
            msg = NetworkUtility.AddInteger(msg, offset);
            msg = NetworkUtility.AddBytes(msg, block);

            return msg;
        }
        public byte[] CreateCancelMessage(uint piece_number, uint offset, uint lenght)
        {
            byte[] msg = new byte[0];

            msg = NetworkUtility.AddInteger(msg, 13);
            msg = NetworkUtility.AddByte(msg, 8);
            msg = NetworkUtility.AddInteger(msg, piece_number);
            msg = NetworkUtility.AddInteger(msg, offset);
            msg = NetworkUtility.AddInteger(msg, lenght);

            return msg;
        }
        public byte[] CreatePortMessage(ushort port)
        {
            byte[] msg = new byte[0];

            msg = NetworkUtility.AddInteger(msg, 3);
            msg = NetworkUtility.AddByte(msg, 9);
            msg = NetworkUtility.AddShort(msg, port);

            return msg;
        }
        public Message DecryptMessage(byte[] byte_message)
        {
            Message message = new Message();

            if (byte_message[0] == 19 && byte_message.Length == 68)
            {
                message.Handshake = true;
                message.Payload = byte_message;
                message.Length = 68;

                return message;
            }
            else
                message.Handshake = false;

            message.Length = NetworkUtility.GetInteger(byte_message, 0);

            if (message.Length == 0)
                return message;

            message.Type = byte_message[4];

            switch (message.Type)
            {
                //choke 
                case 0:

                //unchoke                                                                                   
                case 1:

                //intrested                                                      
                case 2:

                //unintrested                                                             
                case 3: return message;

                //have                
                case 4:
                    {
                        message.Piece = NetworkUtility.GetInteger(byte_message, 5);

                        return message;
                    }

                //bitfield       
                case 5:
                    {
                        message.Payload = NetworkUtility.GetBytes(byte_message, 5, (int)message.Length - 1);

                        return message;
                    }

                //request
                case 6:
                    {
                        message.Piece = NetworkUtility.GetInteger(byte_message, 5);
                        message.Offset = NetworkUtility.GetInteger(byte_message, 9);
                        message.Size = NetworkUtility.GetInteger(byte_message, 13);

                        return message;
                    }

                //piece
                case 7:
                    {
                        message.Piece = NetworkUtility.GetInteger(byte_message, 5);
                        message.Offset = NetworkUtility.GetInteger(byte_message, 9);
                        message.Payload = NetworkUtility.GetBytes(byte_message, 13, (int)message.Length - 9);

                        return message;
                    }

                //cancel
                case 8:
                    {
                        message.Piece = NetworkUtility.GetInteger(byte_message, 5);
                        message.Offset = NetworkUtility.GetInteger(byte_message, 9);
                        message.Size = NetworkUtility.GetInteger(byte_message, 13);

                        return message;
                    }

                //port
                case 9:
                    {
                        message.Port = NetworkUtility.GetShort(byte_message, 5);

                        return message;
                    }
            }

            return default(Message);
        }
    }
}