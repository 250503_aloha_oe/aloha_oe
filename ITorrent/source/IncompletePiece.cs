﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace ITorrent
{
    class IncompletePiece
    {
        private List<Tuple<int, int, bool>> m_blocks;
        private List<Timer> m_timers;

        public const int BlockSize = 16384;

        public byte[] Piece
        {
            get;
            private set;
        }
        
        public IncompletePiece(int piece_size)
        {
            Piece = new byte[piece_size];

            m_blocks = new List<Tuple<int,int, bool>>();
            m_timers = new List<Timer>();

            int blocks_num = piece_size / BlockSize;
            int last_block = piece_size % BlockSize;

            for(int count = 0; count < blocks_num; count++)
            {
                m_blocks.Add(new Tuple<int, int, bool>(count * BlockSize, BlockSize, false));

                m_timers.Add(new Timer(20000));
                m_timers[count].AutoReset = false;
                m_timers[count].Elapsed += TimeElapsed;
            }
            if (last_block > 0)
            {
                m_blocks.Add(new Tuple<int, int, bool>(m_blocks.Count * BlockSize, last_block, false));

                m_timers.Add(new Timer(20000));
                m_timers[blocks_num].AutoReset = false;
                m_timers[blocks_num].Elapsed += TimeElapsed;
            }
        }

        private void TimeElapsed(object sender, ElapsedEventArgs e)
        {
            int index = m_timers.IndexOf(sender as Timer);

            var old = m_blocks[index];

            m_blocks[index] = new Tuple<int, int, bool>(old.Item1, old.Item2, false);
        }

        public void AddBlock(int offset, int length, byte[] block)
        {
            for (int index = 0; index < m_blocks.Count; index++ )
            {
                if (m_blocks[index].Item1 == offset && m_blocks[index].Item2 == length)
                {
                    m_blocks[index] = new Tuple<int, int, bool>(offset, length, true);
                    m_timers[index].Stop();

                    Array.Copy(block, 0, Piece, offset, length);

                    break;
                }
            }
        }
        public Tuple<int, int> GetMissingBlock()
        {
            for (int count = 0; count < m_blocks.Count; count++)
            {
                if (!m_blocks[count].Item3)
                {
                    var old = m_blocks[count];

                    m_blocks[count] = new Tuple<int, int, bool>(old.Item1, old.Item2, true);
                    m_timers[count].Start();

                    return new Tuple<int, int>(old.Item1, old.Item2);
                }
            }

            return null;
        }
        public bool IsComplete()
        {
            for(int count = 0; count < m_blocks.Count; count++)
            {
                if (!m_blocks[count].Item3 || m_timers[count].Enabled)
                    return false;
            }

            return true;
        }
        public void Reset()
        {
            for (int i = 0; i < m_blocks.Count; i++)
            {
                var old = m_blocks[i];

                m_blocks[i] = new Tuple<int, int, bool>(old.Item1, old.Item2, false);
                m_timers[i].Stop();
            }
        }
    }
}
