using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.Web;
using ITorrent.Bencoding;

namespace ITorrent
{

    public class DownloadInfo
    {
        public readonly BDictionary m_dictionary;

        public int DownloadID;
        public byte[] PeerID;
        public uint Port;
        public ulong Downloaded;
        public ulong Uploaded;
        public ulong Left;

        public List<string> Trackers
        {
            get;
            private set;
        }
        public byte[] InfoHash
        {
            get;
            private set;
        }
        public List<long> FilesSize
        {
            get;
            private set;
        }
        public int TotalPiecesAmount
        {
            get;
            private set;
        }
        public List<bool> FilesToDownload
        {
            get;
            private set;
        }
        public int PieceSize
        {
            get;
            private set;
        }
        public bool DHT
        {
            get;
            private set;
        }
        public byte[] this[int index]
        {
            get
            {
                byte[] pieces = m_dictionary["info"]["pieces"].ToByteArray();
                byte[] piece = new byte[20];

                for (int count = 0; count < 20; count++)
                {
                    piece[count] = pieces[index * 20 + count];
                }

                return piece;
            }
        }

        public DownloadInfo(BDictionary dictionary, byte[] peer_id, List<bool> files_to_download, uint port = 6881)
        {
            m_dictionary = dictionary;
            PeerID = peer_id;

            Trackers = new List<string>();

            if (m_dictionary.ContainsKey("announce-list"))
            {
                foreach (var obj in (BList)m_dictionary["announce-list"])
                {
                    Trackers.Add(obj[0].ToString());
                }
            }
            else
            {
                Trackers.Add(m_dictionary["announce"].ToString());
            }

            InfoHash = new SHA1CryptoServiceProvider().ComputeHash(m_dictionary["info"].ToBencodedByteArray());

            Port = port;

            Downloaded = 0;
            Uploaded = 0;
            Left = 0;
           

            TotalPiecesAmount = (m_dictionary["info"]["pieces"] as BString).getByteCount() / 20;

            FilesSize = new List<long>();

            double piece_length = ((BInteger)m_dictionary["info"]["piece length"]).Value;

            if (m_dictionary["info"]["files"] != null)
            {
                for (int count = 0; count < ((BList)m_dictionary["info"]["files"]).Count; count++)
                {
                    FilesSize.Add((m_dictionary["info"]["files"][count]["length"] as BInteger).Value);
                }
            }
            else
            {
                FilesSize.Add((m_dictionary["info"]["length"] as BInteger).Value);
            }

            FilesToDownload = files_to_download;

            for (int i = 0; i < FilesSize.Count; i++)
            {
                if (files_to_download[i])
                    Left += (ulong)FilesSize[i];
            }

            PieceSize = (int)piece_length;
            
        }
    }
}

