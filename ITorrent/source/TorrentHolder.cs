using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using ITorrent.Bencoding;

namespace ITorrent
{
    public class TorrentHolder
    {
        private PiecesHolder m_files;
        private DownloadInfo m_download_info;
        private Bitfield m_bitfield;

        public TorrentHolder(ref DownloadInfo d_info)
        {
            m_download_info = d_info;
            m_bitfield = new Bitfield(m_download_info.TotalPiecesAmount);
            m_files = new PiecesHolder(m_download_info.DownloadID.ToString(), m_download_info.PieceSize);

        }

        public byte[] this[int number]
        {
            get
            {
                if (m_bitfield.HaveBit(number) == false)
                    return null;
                else
                    return m_files.GetPiece(number);
            }
            private set
            {
                m_files.AddPiece(value, number);
            }
        }
        public byte[] GetBlock(int piece_number, int offset, int length)
        {
            if (m_bitfield.HaveBit(piece_number) == false)
                return null;
            else
            {
                byte[] piece = this[piece_number];
                byte[] block = new byte[length];

                Array.Copy(piece, offset, block, 0, length);

                return block;
            }
        }
        public byte[] GetAsBitfield()
        {
            return m_bitfield.BitField;
        }
        public bool AddPiece(int piece_number, byte[] piece)
        {
            byte[] piece_hash = m_download_info[piece_number];
            byte[] new_piece_hash = new SHA1CryptoServiceProvider().ComputeHash(piece);

            for (int i = 0; i < 20; i++)
            {
                if (piece_hash[i] != new_piece_hash[i])
                    return false;
            }

            this[piece_number] = piece;
            m_bitfield.AddBit(piece_number);

            return true;
        }
        public bool HaveAll()
        {
            for(int i = 0; i < m_download_info.TotalPiecesAmount; i++)
            {
                if (IsPieceNeeded(i) && !m_bitfield.HaveBit(i))
                    return false;
            }
            return true;
        }
        public bool Unite()
        {
            if (!HaveAll())
                return false;
            else
            {
                long offset = 0;
                string name;
                for (int i = 0; i < m_download_info.FilesToDownload.Count;i++ )
                {
                    if (m_download_info.FilesToDownload[i]==true)
                    {
                        if (m_download_info.FilesToDownload.Count > 1)
                        {
                            int deep = (m_download_info.m_dictionary["info"]["files"][i]["path"] as BList).Count - 1;
                            name = m_download_info.m_dictionary["info"]["files"][i]["path"][deep].ToString();
                        }
                        else name = m_download_info.m_dictionary["info"]["name"].ToString();
                        
                        m_files.MakeFile(name,offset, m_download_info.FilesSize[i]);
                        
                        
                    }
                    offset += m_download_info.FilesSize[i];
                    

                }
                    
                return true;
            }
        }
        public List<bool> GetNeededPieces()
        {
            List<bool> pieces = new List<bool>();
            int i = 0;

            for (; i < m_download_info.TotalPiecesAmount; i++)
                pieces.Add(IsPieceNeeded(i));

            /*if(pieces[i-2])                      //this code makes following shit : it checks whether we need pre-last piece
                pieces[i-1] = true;                //and depending on that indicicates the need of last piece
            else                                   //this dog-nail is done because IsPieceNeeded method always return false for the last piece
                pieces[i-1] = false;*/

            return pieces;
        }
        private bool IsPieceNeeded(int piece_number)
        {
            long begin = 0;
            long end = 0;
            if(piece_number == m_download_info.TotalPiecesAmount - 1)
            {
                begin = piece_number * m_download_info.PieceSize;
                foreach (var obj in m_download_info.FilesSize)
                    end += obj;
            }
            else 
            {
                begin = piece_number * m_download_info.PieceSize;
                end = begin + m_download_info.PieceSize;
            }
                
            long current_ofset = 0;

            for (int i = 0; i < m_download_info.FilesSize.Count; i++)
            {
                if(m_download_info.FilesToDownload[i] && 
                    ((begin >= current_ofset && begin < current_ofset + m_download_info.FilesSize[i]) ||
                    (end > current_ofset && end <= current_ofset + m_download_info.FilesSize[i])))
                {
                    return true;
                }
                current_ofset += m_download_info.FilesSize[i];
            }

            return false;
        }
    }
}