using System;
using System.Text;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Collections;
using ITorrent.Bencoding;

namespace ITorrent
{

    public enum ClientEvent
    {
	    None,
	    Started,
	    Stopped,
	    Completed,
    }

    public class TrackerRequest
    {
        private readonly DownloadInfo m_download_info;
        private int m_udp_port;
        private int m_active_tracker;

        public TrackerRequest(ref DownloadInfo download_info)
        {
            m_download_info = download_info;
            m_active_tracker = -1;
        }
        public List<Tuple<uint, ushort>> SendRequest(ClientEvent c_event)
        {
            byte[] responce;
            int flag = 0;

            if (m_active_tracker == -1)
            {
                for (int count = 0; count < m_download_info.Trackers.Count; count++)
                {
                    if (m_download_info.Trackers[count].Contains("http://") || m_download_info.Trackers[count].Contains("https://"))
                    {
                        flag = 1;
                        responce = SendRequestOverTcp(count, c_event);
                    }

                    else if (m_download_info.Trackers[count].Contains("udp://"))
                    {
                        responce = SendRequestOverUdp(count, c_event);
                        flag = 0;
                    }

                    else
                        continue;

                    if (responce != null)
                    {
                        m_active_tracker = count;
                        return HandleResponce(responce,flag);
                    }
                }
            }
            else 
            {
                if (m_download_info.Trackers[m_active_tracker].Contains("http://") || m_download_info.Trackers[m_active_tracker].Contains("https://"))
                {
                    responce = SendRequestOverTcp(m_active_tracker, c_event);
                    flag = 1;
                }

                else //(m_download_info.Trackers[m_active_tracker].Contains("udp://"))
                {
                    responce = SendRequestOverUdp(m_active_tracker, c_event);
                    flag = 0;
                }

                return HandleResponce(responce,flag);
            }

            return null;
        }
        private byte[] SendRequestOverTcp(int tracker_number, ClientEvent c_event)
        {
            Stream stream_resp;
            try
            {
                WebRequest request = WebRequest.Create(CreateHttpRequest(tracker_number, c_event));
                request.Credentials = CredentialCache.DefaultCredentials;
                HttpWebResponse responce = (HttpWebResponse)request.GetResponse();
                stream_resp = responce.GetResponseStream();

            }
            catch (WebException ex)
            {
                return null;
            }

            //BinaryReader bin_stream = new BinaryReader(stream_resp);
            MemoryStream ms = new MemoryStream();

            stream_resp.CopyTo(ms);

            return ms.ToArray();
            //ArrayList byter = new ArrayList();
            //try
            //{
            //    while (true)
            //        byter.Add(bin_stream.ReadByte());

            //}
            //catch (EndOfStreamException ex)
            //{

            //}
            //byte[] result = new byte[byter.Count];
            //for (int i = 0; i < byter.Count; i++)
            //    result[i] = (byte)byter[i];
            //if (result[74] == '0')
            //    return null;
            //return result;
        }
        private byte[] SendRequestOverUdp(int tracker_number, ClientEvent c_event)
        {


            string hostname = m_download_info.Trackers[tracker_number];
            hostname = hostname.Substring(6);
            int i = hostname.IndexOf(':');
            i++;
            string port = hostname.Substring(i);
            m_udp_port = Convert.ToInt32(port);
            i = hostname.IndexOf(':');
            hostname = hostname.Substring(0, i);
            byte[] byter = new byte[0];

            byter = NetworkUtility.AddLong(byter, 0x41727101980);//начальный запрос
            byter = NetworkUtility.AddInteger(byter, 0);
            Random rnd = new Random();
            int secret_number = rnd.Next(1000, 10000);
            byter = NetworkUtility.AddInteger(byter, (uint)secret_number);

            IPEndPoint response = null;
            UdpClient udpClient = new UdpClient();
            try
            {
                udpClient.Connect(hostname, m_udp_port);
                udpClient.Send(byter, byter.Length);
                byter = udpClient.Receive(ref response);
            }
            catch (Exception e)
            {

                return null;
            }


            byte[] ifer = new byte[4];
            Array.Copy(byter, 4, ifer, 0, 4);
            Array.Reverse(ifer);
            i = BitConverter.ToInt32(ifer, 0);
            if (i != secret_number)//проверка на вшивость
                return null;
            byter = CreateUdpRequest(byter, c_event);

            try
            {
                udpClient.Send(byter, byter.Length);
                byter = udpClient.Receive(ref response);
            }
            catch (Exception ex)
            {
                return null;
            }

            udpClient.Close();
            return byter;






        }
        private List<Tuple<uint, ushort>> HandleResponce(byte[] responce,int flag)
        {
            int i = 20;
            byte[] ip_peer = new byte[4];
            byte[] port_peer = new byte[2];

            List<Tuple<uint, ushort>> list_peers = new List<Tuple<uint, ushort>>();
            if (responce == null)
                return null;

            if (flag==0)
                i = 20;
            else i = 76;

            while (i < responce.Length)
            {
                Array.Copy(responce, i, ip_peer, 0, 4);
                i += 4;
                Array.Copy(responce, i, port_peer, 0, 2);
                i += 2;
                Array.Reverse(ip_peer);
                Array.Reverse(port_peer);
                list_peers.Add(Tuple.Create(BitConverter.ToUInt32(ip_peer, 0), BitConverter.ToUInt16(port_peer, 0)));
            }

            return list_peers;


        }
        private string CreateHttpRequest(int tracker_number, ClientEvent c_event, bool compact = true, bool no_peer_id = true)
        {
            StringBuilder request = new StringBuilder();
            string tracker = m_download_info.Trackers[tracker_number];
            bool have_params;

            if (tracker.Contains("?"))
                have_params = true;
            else
                have_params = false;

            request.Append(m_download_info.Trackers[tracker_number]);
            request.Append(GetKeyValuePair("info_hash", UrlEncode(m_download_info.InfoHash), !have_params));
            request.Append(GetKeyValuePair("peer_id", UrlEncode(m_download_info.PeerID), false));
            request.Append(GetKeyValuePair("port", m_download_info.Port.ToString(), false));
            request.Append(GetKeyValuePair("uploaded", m_download_info.Uploaded.ToString(), false));
            request.Append(GetKeyValuePair("downloaded", m_download_info.Downloaded.ToString(), false));
            request.Append(GetKeyValuePair("left", m_download_info.Left.ToString(), false));
            request.Append(GetKeyValuePair("compact", Convert.ToInt32(compact).ToString(), false));
            request.Append(GetKeyValuePair("no_peer_id", Convert.ToInt32(no_peer_id).ToString(), false));
            if (c_event != ClientEvent.None)
                request.Append(GetKeyValuePair("event", c_event.ToString().ToLower(), false));

            return request.ToString();
        }

        private string GetKeyValuePair(string key, string value, bool is_first)
        {
            return is_first ? ("?" + key + "=" + value) : ("&" + key + "=" + value);
        }
        private string UrlEncode(byte[] data)
        {
            string str = null;
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == 45 || data[i] == 46 || data[i] == 95 || data[i] == 126 ||   // we should encode everything except '_', '.', '~', '-',
                    (data[i] > 47 && data[i] < 58) || (data[i] > 64 && data[i] < 91) ||   // 'A-Z', 'a-z' and '0-9' to '%nn' ,
                    (data[i] > 96 && data[i] < 123))                                      // where nn - hexademical value 
                {
                    str += (char)data[i];
                }
                else
                {
                    str += ("%" + ((int)data[i]).ToString("X2"));
                }
            }
            return str;
        }
        private byte[] CreateUdpRequest(byte[] byter, ClientEvent c_event, uint peer_number = 50)
        {
            byte[] request = new byte[8];
            Array.Copy(byter, 8, request, 0, 8);

            request = NetworkUtility.AddInteger(request, 1); //action
            request = NetworkUtility.AddInteger(request, m_download_info.Port); //transaction id
            byte[] request_final = new byte[request.Length + 40];
            request.CopyTo(request_final, 0);
            m_download_info.InfoHash.CopyTo(request_final, 16);


            request = m_download_info.PeerID;
            request.CopyTo(request_final, 36);
            request_final = NetworkUtility.AddLong(request_final, m_download_info.Downloaded);
            request_final = NetworkUtility.AddLong(request_final, m_download_info.Left);
            request_final = NetworkUtility.AddLong(request_final, m_download_info.Uploaded);

            uint e;
            switch(c_event)
            {
                case ClientEvent.Completed : e = 1; break;
                case ClientEvent.Started: e = 2; break;
                case ClientEvent.Stopped : e = 3; break;
                default: e = 0; break;
            }

            request_final = NetworkUtility.AddInteger(request_final, e);// Event=download startet
            request_final = NetworkUtility.AddInteger(request_final, 0); // Ipv6
            request_final = NetworkUtility.AddInteger(request_final, 0);
            request_final = NetworkUtility.AddInteger(request_final, peer_number);
            request_final = NetworkUtility.AddShort(request_final, (ushort)m_download_info.Port);
            return request_final;
        }
    }
}