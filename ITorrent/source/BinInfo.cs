using System;
using System.IO;

namespace ITorrent
{
    public class PiecesHolder
    {
        private string m_file_name;
        private string m_expansion;
        private int m_size;

        public PiecesHolder(string file_name, int size)
        {
            m_file_name = file_name;
            m_expansion = "ao";
            m_size = size;
        }

        public void AddPiece(byte[] piece, int id)
        {
            BinaryWriter bin_wr = new BinaryWriter(new FileStream(m_file_name + "_id" + id + ".ao", FileMode.Create));
            bin_wr.Write(piece);
            bin_wr.Close();
        }

        public byte[] GetPiece(int id)
        {
            BinaryReader bin_rd = new BinaryReader(new FileStream(m_file_name + "_id" + id + ".ao", FileMode.Open));
            byte[] piece = new byte[m_size];
            piece = bin_rd.ReadBytes(m_size);
            bin_rd.Close();
            return piece;
        }
        public void MakeFile(string file_name, long offset, long length)
        {
            BinaryReader bin_reader = new BinaryReader(File.Create(file_name));
            int id = (int)offset/ m_size;
            byte[] byter = null;
            int ost=(int)offset - m_size * id;
            long value = 0;
            long sizeRead = m_size;
            while (true)
            {
                
                if ((value + m_size) < length)
                {
                    sizeRead = m_size - ost;
                    value += sizeRead;
                   

                }
                else
                {
                    if (value == length)
                        break;
                    sizeRead = length - value;
                    value += sizeRead;


                }
                byter = GetPiece((int)id);
                bin_reader.BaseStream.Write(byter, ost, (int)sizeRead);
                ost = 0;

                id++;
            }
            bin_reader.Close();

        }
        public void DeleteAllTemp()
        {

        }
    }
}