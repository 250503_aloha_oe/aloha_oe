using System;
using System.Net;
using System.Net.Sockets;
using System.Timers;
using System.Collections.Generic;
using System.Collections;

namespace ITorrent
{

    public class Peer : IEquatable<Peer>, IComparable<Peer>
    {
        private Socket m_socket;
        private Timer m_timer;
        private List<byte[]> m_messages;
        private byte[] m_peer_id;
        private int m_expected_len;
        private byte[] m_incomplete_message;

        public uint Ip
        {
            get;
            private set;
        }
        public ushort Port
        {
            get;
            private set;
        }
        public bool Connected
        {
            get
            {
                return m_socket.Connected;
            }
        }

        public bool AmChocking;
        public bool PeerChocking;
        public bool AmIntrested;
        public bool PeerIntrested;
        public bool IsActive;
        public bool IsHandshaked;
        public bool HandshakeSent;

        public Bitfield Bitfield
        {
            get;
            private set;
        }

        public Peer(uint ip, ushort port, int pieces_number)
            : this(new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp), pieces_number)
        {
            Ip = ip;
            Port = port;
            IsHandshaked = false;
            HandshakeSent = false;

            TryConnect();
        }
        public Peer(Socket socket, int pieces_number)
        {
            m_socket = socket;
            m_messages = new List<byte[]>();

            AmChocking = true;
            PeerChocking = true;
            AmIntrested = false;
            PeerIntrested = false;

            m_socket.Blocking = false;
            IsActive = true;
            IsHandshaked = true;
            HandshakeSent = true;

            Bitfield = new Bitfield(pieces_number);

            m_timer = new Timer(120000);
            m_timer.AutoReset = false;
            m_timer.Elapsed += TimeoutElapsed;
            m_timer.Start();

            m_expected_len = 0;
        }

        ~Peer()
        {
            m_socket.Blocking = true;

            if (m_socket.Connected)
            {
                try
                {
                    m_socket.Disconnect(false);
                }
                catch (Exception e)
                {

                }
            }
            m_socket.Close();
        }

        private void TimeoutElapsed(object sender, ElapsedEventArgs e)
        {
            IsActive = false;
        }
        private void ResetTimer()
        {
            m_timer.Stop();
            m_timer.Start();
        }
        public void TryConnect()
        {
            try
            {
                m_socket.Connect(new IPAddress(NetworkUtility.AddInteger(new byte[0], Ip)), Port);
            }
            catch (SocketException exc)
            {
            }
        }
        public void Send(byte[] msg)
        {
             m_messages.Add(msg);
        }
        public void Update()
        {
            if (m_socket.Connected)
            {
                if (HandshakeSent)
                {
                    try
                    {
                        m_socket.Send(m_messages[0]);
                    }
                    catch (SocketException e)
                    {

                    }
                    m_messages.RemoveAt(0);
                    HandshakeSent = false;
                }
                else if(IsHandshaked)
                {
                    try
                    {
                        foreach (var m in m_messages)
                            m_socket.Send(m);

                        m_messages.Clear();
                    }
                    catch (SocketException e)
                    {

                    }
                }
            }
        }

        public bool Equals(Peer other)
        {
            return this.m_socket.RemoteEndPoint.Equals(other.m_socket.RemoteEndPoint);
        }

        public int CompareTo(Peer other)
        {
            if (other.PeerChocking == true && this.PeerChocking == false)
                return -1;
            else if (other.PeerChocking == false && this.PeerChocking == true)
                return 1;
            else if (other.PeerChocking == this.PeerChocking)
            {
                for (int i = 0; i < Bitfield.BitField.Length; i++)
                {
                    if (other.Bitfield.BitField[i] < this.Bitfield.BitField[i])
                        return 1;
                    else if (other.Bitfield.BitField[i] > this.Bitfield.BitField[i])
                        return -1;
                }
            }
            return 0;
        }

        public byte[] Receive()
        {
            if(!m_socket.Connected)
                return null;
            if (m_socket.Available >= 4)
            {
                ResetTimer();

                if(m_socket.Available >= 68 && !IsHandshaked)
                {
                    byte[] handshake = new byte[68];

                    m_socket.Receive(handshake, 68, SocketFlags.None);

                    IsHandshaked = true;
                    return handshake;
                }
                if(m_expected_len != 0)
                {
                    int aval = m_socket.Available;

                    if (m_socket.Available < m_expected_len)
                    {
                        m_socket.Receive(m_incomplete_message, m_incomplete_message.Length - m_expected_len, aval, SocketFlags.None);
                        m_expected_len -= aval;

                        return null; 
                    }                
                    else
                    {
                        m_socket.Receive(m_incomplete_message, m_incomplete_message.Length - m_expected_len, m_expected_len, SocketFlags.None);

                        m_expected_len = 0;

                        return m_incomplete_message;
                    }
                }
                byte[] msg_len = new byte[4];

                m_socket.Receive(msg_len, 4, SocketFlags.None);

                int len = (int)NetworkUtility.GetInteger(msg_len, 0);

                if (len == 0)
                    return msg_len;

                if(len < 0)
                {
                    IsActive = false;
                    return null;
                }

                if (m_socket.Available < len)
                {
                    try
                    {
                        m_incomplete_message = new byte[len + 4];
                    }
                    catch(Exception e)
                    {
                        IsActive = false;
                        return null;
                    }
                    msg_len.CopyTo(m_incomplete_message, 0);
                    m_expected_len = len;
                    return null;
                }
                    

                byte[] msg;
                try
                {
                    msg = new byte[len + 4];
                }
                catch(Exception e)
                {
                    IsActive = false;
                    return null;
                }
                

                msg_len.CopyTo(msg, 0);
                m_socket.Receive(msg, 4, len, SocketFlags.None);

                return msg;
            }
            else
                return null;
        }
    }
}