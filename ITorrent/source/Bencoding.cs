using System;
using System.Collections.Generic;
using System.Text;

namespace ITorrent.Bencoding
{
    public enum BType
    {
        Integer,
        String,
        List,
        Dictionary
    }
    public interface IBElement
    {
        string ToBencodedString();
        byte[] ToByteArray();
        byte[] ToBencodedByteArray();
        int getByteCount();
        int getBencodedByteCount();
        BType getBType();

        IBElement this[int number]
        {
            get;
        }
        IBElement this[string key]
        {
            get;
        }
    }
    public class BInteger : IBElement, IComparable<BInteger>
    {
        public static implicit operator BInteger(long value)
        {
            return new BInteger(value);
        }
        public long Value
        {
            get;
            set;
        }
        public BInteger(long value)
        {
            this.Value = value;
        }
        public string ToBencodedString()
        {
            return ("i" + this.Value.ToString() + "e");
        }
        public byte[] ToByteArray()
        {
            return Encoding.UTF8.GetBytes(Value.ToString());
        }
        public Byte[] ToBencodedByteArray()
        {
            byte[] temp = this.ToByteArray();
            byte[] bint = new byte[temp.Length + 2];
            temp.CopyTo(bint, 1);
            bint[0] = (byte)'i';
            bint[bint.Length - 1] = (byte)'e';
            return bint;
        }
        public int getByteCount()
        {
            return Encoding.UTF8.GetByteCount(Value.ToString());
        }
        public int getBencodedByteCount()
        {
            return getByteCount() + 2;
        }

        public int CompareTo(BInteger obj)
        {
            return this.Value.CompareTo(obj.Value);
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }
        public override string ToString()
        {
            return this.Value.ToString();
        }
        public override bool Equals(object obj)
        {
            return this.Value.Equals(((BInteger)obj).Value);
        }
        public BType getBType()
        {
            return BType.Integer;
        }
        public IBElement this[int number]
        {
            get
            {
                return null;
            }
        }
        public IBElement this[string key]
        {
            get
            {
                return null;
            }
        }
    }
    public class BString : IBElement, IComparable<BString>
    {
        public static implicit operator BString(string str)
        {
            return new BString(str);
        }
        public static implicit operator BString(byte[] bytes)
        {
            return new BString(bytes);
        }
        public byte[] Value
        {
            get;
            set;
        }
        public BString(string str)
        {
            this.Value = Encoding.UTF8.GetBytes(str);
        }
        public BString(byte[] bytes)
        {
            this.Value = bytes;
        }
        public string ToBencodedString()
        {
            return (Value.Length.ToString() + ":" + Encoding.UTF8.GetString(Value));
        }
        public byte[] ToByteArray()
        {
            return Value;
        }
        public byte[] ToBencodedByteArray()
        {
            byte[] size = Encoding.UTF8.GetBytes(Value.Length.ToString() + ":");
            byte[] bstring = new byte[size.Length + Value.Length];
            size.CopyTo(bstring, 0);
            Value.CopyTo(bstring, size.Length);
            return bstring;
        }
        public int getByteCount()
        {
            return Value.Length;
        }
        public int getBencodedByteCount()
        {
            return Value.Length.ToString().Length + 1 + Value.Length;
        }
        public int CompareTo(BString obj)
        {
            for (int i = 0; i < obj.Value.Length && i < this.Value.Length; i++)
            {
                if (obj.Value[i] < Value[i])
                    return 1;
                if (obj.Value[i] > Value[i])
                    return -1;
                if (obj.Value.Length - 1 == i && Value.Length > obj.Value.Length)
                    return 1;
                if (Value.Length - 1 == i && Value.Length < obj.Value.Length)
                    return -1;
            }
            return 0;
        }
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return Value.Equals(((BString)obj).Value);
        }
        public override string ToString()
        {
            return Encoding.UTF8.GetString(Value);
        }
        public BType getBType()
        {
            return BType.String;
        }
        public IBElement this[int number]
        {
            get
            {
                return null;
            }
        }
        public IBElement this[string key]
        {
            get
            {
                return null;
            }
        }
    }
    public class BList : List<IBElement>, IBElement
    {

        public void Add(int value)
        {
            base.Add(new BInteger(value));
        }
        public void Add(string value)
        {
            base.Add(new BString(value));
        }
        public byte[] ToByteArray()
        {
            byte[] temp;
            byte[] curr;
            byte[] storage = new byte[0];
            foreach (var obj in base.ToArray())
            {
                curr = obj.ToByteArray();
                temp = new byte[storage.Length + curr.Length];
                storage.CopyTo(temp, 0);
                curr.CopyTo(temp, storage.Length);
                storage = temp;
            }
            return storage;
        }
        public byte[] ToBencodedByteArray()
        {
            int index = 1;
            byte[] blist = new byte[getBencodedByteCount()];
            byte[] temp;

            blist[0] = (byte)'l';
            foreach (var obj in base.ToArray())
            {
                temp = obj.ToBencodedByteArray();
                temp.CopyTo(blist, index);
                index += temp.Length;
            }
            blist[blist.Length - 1] = (byte)'e';
            return blist;
        }
        public string ToBencodedString()
        {
            StringBuilder temp = new StringBuilder("l");
            foreach (var obj in base.ToArray())
            {
                temp.Append(obj.ToBencodedString());
            }
            temp.Append("e");
            return temp.ToString();
        }
        public int getByteCount()
        {
            int count = 0;
            foreach (var obj in base.ToArray())
            {
                count += obj.getByteCount();
            }
            return count;
        }
        public int getBencodedByteCount()
        {
            int count = 0;
            foreach (var obj in base.ToArray())
            {
                count += obj.getBencodedByteCount();
            }
            return count + 2;
        }
        public BType getBType()
        {
            return BType.List;
        }
        //new public IBElement this[int number]
        //{
        //    get
        //    {
        //        if (this.Count > number)
        //            return this[number];
        //        else
        //            return null;
        //    }
        //}
        public IBElement this[string key]
        {
            get
            {
                return null;
            }
        }
    }
    public class BDictionary : SortedDictionary<BString, IBElement>, IBElement
    {
        public void Add(string key, IBElement value)
        {
            base.Add(new BString(key), value);
        }
        public void Add(string key, int value)
        {
            base.Add(new BString(key), new BInteger(value));
        }
        public void Add(string key, string value)
        {
            base.Add(new BString(key), new BString(value));
        }
        public void Add(KeyValuePair<BString, IBElement> pair)
        {
            base.Add(pair.Key, pair.Value);
        }
        public IBElement this[string key]
        {
            get
            {
                if (base.ContainsKey(key))
                    return this[new BString(key)];
                else
                    return null;
            }
            set
            {
                this[new BString(key)] = value;
            }
        }
        public IBElement this[int number]
        {
            get
            {
                return null;
            }
        }
        public string ToBencodedString()
        {
            StringBuilder temp = new StringBuilder("d");
            SortedDictionary<BString, IBElement>.KeyCollection.Enumerator key = base.Keys.GetEnumerator();
            SortedDictionary<BString, IBElement>.ValueCollection.Enumerator value = base.Values.GetEnumerator();
            for (int i = 0; i < base.Count; i++)
            {
                key.MoveNext();
                value.MoveNext();
                temp.Append(key.Current.ToBencodedString());
                temp.Append(value.Current.ToBencodedString());
            }
            temp.Append("e");
            return temp.ToString();
        }
        public byte[] ToByteArray()
        {
            byte[] storage = new byte[0];
            byte[] value;
            byte[] key;
            byte[] temp;

            SortedDictionary<BString, IBElement>.KeyCollection.Enumerator key_en = base.Keys.GetEnumerator();
            SortedDictionary<BString, IBElement>.ValueCollection.Enumerator value_en = base.Values.GetEnumerator();

            for (int i = 0; i < base.Count; i++)
            {
                value_en.MoveNext();
                key_en.MoveNext();

                key = key_en.Current.ToByteArray();
                value = value_en.Current.ToByteArray();

                temp = new byte[storage.Length + key.Length + value.Length];
                storage.CopyTo(temp, 0);
                key.CopyTo(temp, storage.Length);
                value.CopyTo(temp, storage.Length + key.Length);
                storage = temp;
            }
            return storage;
        }
        public byte[] ToBencodedByteArray()
        {
            var en = base.GetEnumerator();
            byte[] bdict = new byte[getBencodedByteCount()];
            int index = 1;
            byte[] temp;
            bdict[0] = (byte)'d';
            for (int i = 0; i < base.Count; i++)
            {
                en.MoveNext();
                temp = en.Current.Key.ToBencodedByteArray();
                temp.CopyTo(bdict, index);
                index += temp.Length;

                temp = en.Current.Value.ToBencodedByteArray();
                temp.CopyTo(bdict, index);
                index += temp.Length;
            }
            bdict[bdict.Length - 1] = (byte)'e';
            return bdict;
        }
        public int getByteCount()
        {
            int count = 0;
            var en = base.GetEnumerator();
            for (int i = 0; i < base.Count; i++)
            {
                en.MoveNext();
                count += en.Current.Key.getByteCount();
                count += en.Current.Value.getByteCount();
            }
            return count;
        }
        public int getBencodedByteCount()
        {
            int count = 0;
            var en = base.GetEnumerator();
            for (int i = 0; i < base.Count; i++)
            {
                en.MoveNext();
                count += en.Current.Key.getBencodedByteCount();
                count += en.Current.Value.getBencodedByteCount();
            }
            return count + 2;

        }
        public BType getBType()
        {
            return BType.Dictionary;
        }
    }
}