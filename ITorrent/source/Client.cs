﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITorrent.GUI;

namespace ITorrent
{
    class Client
    {
        private DownloadCreator m_creator;
        private MainWindow m_gui;

        public Client()
        {
            m_creator = new DownloadCreator();

            m_gui = new MainWindow();
            m_gui.AddDelegate(m_creator.AddDownload);
            m_gui.AddCloseDel(m_creator.CloseAllDownloads);
        }


        public void Run()
        {
            m_gui.ShowDialog();
        }
    }
}

