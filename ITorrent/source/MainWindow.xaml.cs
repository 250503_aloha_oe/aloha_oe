﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using GalaSoft.MvvmLight;
using System.IO;
using ITorrent.Bencoding;
using System.Collections.Generic;
using ITorrent;

namespace ITorrent.GUI
{
    public delegate void DownloadAdd(BDictionary dict, List<bool> files_to_download, 
                                        out Pause pause_del, 
                                        out Continue continue_del,
                                        ChangeProgress p_del,
                                        ChangeSpeed s_del); 
    public delegate void Pause();
    public delegate void Continue();
    public delegate void CloseApp();

    public class Downloading : ObservableObject
    {
        private float _progress;
        private float _speed;

        public string Name { get; set; }
        public string Size { get; set; }
        public float Progress
        {
            get
            {
                return _progress;
            }
            set
            {
                _progress = value;
                RaisePropertyChanged(() => Progress);
            }
        }
        public float Speed
        {
            get
            {
                return _speed;
            }
            set
            {
                _speed = value;
                RaisePropertyChanged(() => Speed);
            }
        }
    }

    public partial class MainWindow : Window
    {
        private ObservableCollection<Downloading> items = new ObservableCollection<Downloading>();

        private DownloadAdd m_del;
        private CloseApp m_close_del;

        private List<Pause> m_pauses;
        private List<Continue> m_continues;

        public MainWindow()
        {
            InitializeComponent();

            m_pauses = new List<Pause>();
            m_continues = new List<Continue>();
        }

        ~MainWindow()
        {
            //m_close_del();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Listview.ItemsSource = items;
        }

        public void AddCloseDel(CloseApp del)
        {
            m_close_del = del;
        }

        private void AddTorrent(object sender, RoutedEventArgs e)
        {
            List<bool> selectedFiles = new List<bool>();

            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "(.torrent)|*.torrent";

            openDialog.ShowDialog();

            if (openDialog.FileName.Length == 0)
                return;
            else
            {
                BinaryReader br = new BinaryReader(File.Open(openDialog.FileName, FileMode.Open));

                var dict = new BCoder().Decode(br.ReadBytes(100000)) as BDictionary;

                br.Close();

                if (Exist(dict["info"]["name"].ToString()))
                {
                    MessageBox.Show("Torrent already exist");
                }
                else
                {
                    if ((dict["info"] as BDictionary).ContainsKey("files"))
                    {
                        NewTorrent wndw = new NewTorrent(dict, ref selectedFiles);
                        wndw.ShowDialog();

                        if (wndw.Add)
                        {
                            long size = 0;

                            int count = (dict["info"]["files"] as BList).Count;

                            for (int i = 0; i < count; i++)
                            {
                                size += (dict["info"]["files"][i]["length"] as BInteger).Value;
                            }

                            AddDownload(dict["info"]["name"].ToString(), size, 0, 0);

                            wndw.Add = false;

                            Pause new_pause;
                            Continue new_continue;

                            m_del(dict, selectedFiles, out new_pause, out new_continue, ChangeProgress, ChangeSpeed);


                            m_pauses.Add(new_pause);
                            m_continues.Add(new_continue);
                        }
                    }
                    else
                    {
                        selectedFiles.Add(true);
                        AddDownload(dict["info"]["name"].ToString(), (dict["info"]["length"] as BInteger).Value, 0, 0);

                        Pause new_pause;
                        Continue new_continue;

                        m_del(dict, selectedFiles, out new_pause, out new_continue, ChangeProgress, ChangeSpeed);


                        m_pauses.Add(new_pause);
                        m_continues.Add(new_continue);

                    }

                   
                }
            }
        }

        private bool Exist(string name)
        {
            bool exist = false;

            foreach (var item in items)
            {
                if (item.Name.Equals(name))
                    exist = true;
            }

            return exist;
        }

        private void ContinueAllTorrents(object sender, RoutedEventArgs e)
        {
            foreach (var del in m_continues)
                del();
        }

        private void PauseAllTorrents(object sender, RoutedEventArgs e)
        {
            foreach (var del in m_pauses)
                del();
        }

        private void ExitProgram(object sender, RoutedEventArgs e)
        {
            m_close_del();
            this.Close();
        }

        private void About(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Anton Protko - teamleader\nNikita Gurinovich - good boy\nVlad Volchkevich - real pizdabol");
        }

        private void StartDownload(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            Downloading a = b.CommandParameter as Downloading;
            int i = 0;

            foreach (var item in items)
            {
                if (item.Equals(a))
                {
                    m_continues[i]();
                    break;
                }
                i++;
            }
        }

        private void PauseDownload(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            Downloading a = b.CommandParameter as Downloading;
            int i = 0;

            foreach (var item in items)
            {
                if (item.Equals(a))
                {
                    m_pauses[i]();
                    break;
                }
                i++;
            }
        }

        private void DeleteDownload(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            Downloading a = b.CommandParameter as Downloading;
            items.Remove(a);
        }

        public void AddDelegate(DownloadAdd del)
        {
            m_del = del;
        }

        private void AddDownload(string name, long size, float progress, float speed)
        {
            items.Add(new Downloading() { Name = name, Size = FormatSize(size), Progress = progress, Speed = speed });
        }

        public void ChangeProgress(string name, float progress)
        {
            foreach (var item in items)
            {
                if (item.Name.Equals(name))
                    item.Progress = progress;
            }
        }
        public void ChangeSpeed(string name, float speed)
        {
            foreach (var item in items)
            {
                if (item.Name.Equals(name))
                    item.Speed = speed;
            }
        }

        private string FormatSize(long size)
        {
            double result = 0;

            if (size < 1000)
                return size.ToString("F2") + " B";
            else
            {
                result = size / Math.Pow (2, 10);
                if (result < 1000)
                    return result.ToString("F2") + " KB";
                else
                {
                    result = size / Math.Pow(2, 20);
                    if (result < 1000)
                        return result.ToString("F2") + " MB";
                    else
                    {
                        result = size / Math.Pow(2, 30);
                        return result.ToString("F2") + " GB";
                    }                     
                }
            }
        }

    }
}