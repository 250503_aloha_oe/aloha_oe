using System;
using System.Reflection;
using ITorrent;
using System.Threading;

public class Program
{
    [System.STAThreadAttribute()]
    //[System.Diagnostics.DebuggerNonUserCodeAttribute()]
    static public void Main()
    {
        Bitfield t = new Bitfield(20);
        for (int i = 0; i < 20;i++ )
            t.AddBit(i);
        var a = t.HaveBit(179);
        AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

        Client client = new Client();

        client.Run();
    }

    private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
    {
        using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ITorrent.source.dependencies.GalaSoft.MvvmLight.dll"))
        {
            var a = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            byte[] assembly_data = new byte[stream.Length];

            stream.Read(assembly_data, 0, assembly_data.Length);
            return Assembly.Load(assembly_data);
        }
    }
}

